# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import socket
import struct
import time

#define the servers IP address and portfi
server_ip = '192.168.178.22'
server_port = 2010

#initial data
data = int('1000000001100011110000', 2)
bit_0 = False;
#create a socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    #connect to server
    client_socket.connect((server_ip, server_port))

    #send data func
    def send_data(data):
        packed_data = struct.pack('i', data)
        client_socket.sendall(packed_data)

    #read data func
    def read_data():
        data = client_socket.recv(4)

        if data:
            received_data = int.from_bytes(data,'little', signed=False)
            bit0 = received_data & 0x01
            print(f'received data: {received_data}')
        else:
            print('connection closed')
            raise SystemExit

    #loop each 100ms
    while True:
        send_data(data)
        print(f'sent data: {data}')
        read_data()
        #data = data & bit_0
        time.sleep(0.1)

except socket.error as e:
    print(f'socket error: {e}')
except KeyboardInterrupt:
    print('program terminated')

# Close socket
client_socket.close()